/**
* Create by goland
* Author: 墨城
* Create Time: 2021-10-09 14:21
* Maintainers:
 */
package golang_tools

import (
	"fmt"
	"io/ioutil"
	"os"
)

// 判断所给路径文件/文件夹是否存在
func Exists(path string) bool {
	//os.Stat获取文件信息
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

// 判断所给路径是否为文件夹
func IsDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

// 判断所给路径是否为文件
func IsFile(path string) bool {
	return !IsDir(path)
}

// 读取目录
//func ReadDir(d string) {
//
//}

func ReadFile(f string) ([]byte, error) {
	if Exists(f) == false || IsFile(f) == false {
		return []byte{}, fmt.Errorf("%s", "file not exists")
	}
	file, err := os.OpenFile(f, os.O_RDONLY, 0600)
	if err != nil {
		return []byte{}, err
	}
	content, readErr := ioutil.ReadAll(file)
	return content, readErr
}

//func ReadFileOnLine() ([]string,error) {
//
//}

//func WriteFile(f string,m []byte) error{
//     f,err := os.Open(f,os.O_WRONLY|)
//}
// 单行写入
//func WriteFileOnLine(f string,m []byte) error{
//
//}
