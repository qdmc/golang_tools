/**
* @Time : 2021/11/5 下午9:21
* @Author : qcmc
* @File : Itimer.go
* @Software: GoLand
 */
package task

import (
	"fmt"
	"gitee.com/qdmc/golang_tools/ids"
	"sync"
	"time"
)

type TimerTask struct {
	Id          string
	Duration    time.Duration
	Timer       Itimer
	T           *time.Timer
	ControlChan chan bool
}

func (t *TimerTask) doTimer() {
	defer func() {
		delete(TimerTaskMap, t.Id)
	}()
	select {
	case <-t.T.C:
		t.Timer.Handle()
		return
	case <-t.ControlChan:
		return
	}
}

var TimerTaskMap = make(map[string]*TimerTask)
var tMutex sync.Mutex

type TimerInfo struct {
	DataBase []byte
	New      func() Itimer
}

type Itimer interface {
	Info() *TimerInfo
	Handle()
}

func RegisterTimer(t Itimer, d time.Duration) string {
	info := t.Info()
	if info.New == nil {
		panic("missing Task.Timer.New ")
	}
	task := &TimerTask{
		Id:          ids.New(),
		Duration:    d,
		Timer:       info.New(),
		T:           time.NewTimer(d),
		ControlChan: make(chan bool),
	}
	tMutex.Lock()
	defer tMutex.Unlock()
	TimerTaskMap[task.Id] = task
	go task.doTimer()
	return task.Id
}

func ResetTimer(taskId string, d time.Duration) (bool, error) {
	if t, ok := TimerTaskMap[taskId]; ok {
		return t.T.Reset(d), nil
	} else {
		return false, fmt.Errorf("Timer  not exist")
	}
}

func StopTimer(taskId string) (bool, error) {
	if t, ok := TimerTaskMap[taskId]; ok {
		res := t.T.Stop()
		t.ControlChan <- true
		return res, nil
	} else {
		return false, fmt.Errorf("Timer  not exist")
	}
}
