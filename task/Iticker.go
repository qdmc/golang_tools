/**
* @Time : 2021/11/5 下午11:17
* @Author : qcmc
* @File : Iticker.go
* @Software: GoLand
 */
package task

import (
	"fmt"
	"gitee.com/qdmc/golang_tools/ids"
	"sync"
	"time"
)

type TickerTask struct {
	Id          string
	Duration    time.Duration
	Timer       Iticker
	T           *time.Ticker
	ControlChan chan bool
}

func (t *TickerTask) doTicker() {
	defer func() {
		delete(TickerTaskMap, t.Id)
	}()
	for {
		select {
		case <-t.T.C:
			go t.Timer.Handle()
		case <-t.ControlChan:
			return
		}
	}
}

type Iticker interface {
	Info() *TickerInfo
	Handle()
}

type TickerInfo struct {
	DataBase []byte
	New      func() Iticker
}

var TickerTaskMap = make(map[string]*TickerTask)
var mu sync.Mutex

func RegisterTicker(t Iticker, d time.Duration) string {
	info := t.Info()
	if info.New == nil {
		panic("missing Task.Timer.New ")
	}
	task := &TickerTask{
		Id:          ids.New(),
		Duration:    d,
		Timer:       info.New(),
		T:           time.NewTicker(d),
		ControlChan: make(chan bool),
	}
	mu.Lock()
	defer mu.Unlock()
	TickerTaskMap[task.Id] = task
	go task.doTicker()
	return task.Id
}

func ResetTicker(taskId string, d time.Duration) error {
	if t, ok := TickerTaskMap[taskId]; ok {
		t.T.Reset(d)
		return nil
	} else {
		return fmt.Errorf("Timer  not exist")
	}
}

func StopTicker(taskId string) error {
	if t, ok := TickerTaskMap[taskId]; ok {
		t.T.Stop()
		t.ControlChan <- true
		return nil
	} else {
		return fmt.Errorf("Timer  not exist")
	}
}
