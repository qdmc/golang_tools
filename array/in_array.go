/**
* @Time : 2021/10/15 下午10:22
* @Author : qcmc
* @File : in_array.go
* @Software: GoLand
 */
package array

func InArrayInt(need int, items ...int) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}

func InArrayUint(need uint, items ...uint) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
func InArrayInt8(need int8, items ...int8) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
func InArrayUint8(need uint8, items ...uint8) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}

func InArrayInt16(need int16, items ...int16) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
func InArrayUint16(need uint16, items ...uint16) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
func InArrayInt32(need int32, items ...int32) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
func InArrayUint32(need uint32, items ...uint32) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}

func InArrayInt64(need int64, items ...int64) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}

func InArrayUint64(need uint64, items ...uint64) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}

func InArrayString(need string, items ...string) bool {
	for _, v := range items {
		if v == need {
			return true
		}
	}
	return false
}
